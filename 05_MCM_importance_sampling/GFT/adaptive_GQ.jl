using Plots, QuadGK

# get the parameters
include("utils/GFT_param.jl")
include("utils/quadgk.jl")
using .GFT_param_mod: rlb, rlb_aut, g
using .quadgk_mod: rtol_test, order_test, rtol_order_report

# simple adaptive gauss quadrature
W, W_err = quadgk(g, rlb, 1)
W_aut, W_err_aut = quadgk(g, rlb_aut, 1)
GFT = W / W_aut
@show W_err, W_err_aut, GFT


# inspect the impact of parameters:rtol
# briefly sighting
test_num = 100000
bound = (1e-2, 1e-16)
GFT = quadgk_rtol_test(bound, test_num)
plot(1:test_num, GFT, label="GFT", title="impact of rtol(wide range)")

# adjust smaller
test_num = 10000
bound = (1e-7, 1e-16)
GFT = quadgk_rtol_test(bound, test_num)
plot(1:test_num, GFT, label="GFT", title="impact of rtol(smaller range)")

# adjust even smaller
test_num = 1000
bound = (1e-10, 1e-16)
GFT = quadgk_rtol_test(bound, test_num)
plot(1:test_num, GFT, label="GFT", title="impact of rtol(even smaller range)")


# inspcet the impact of parameters:order
# briefly sighting
step = 1
bound = (2, 20)
GFT = quadgk_order_test(bound, step)
plot(1:length(GFT), GFT, label="GFT", title="impact of order(briefly sight)")

# wide range
step = 1
bound = (5, 50)
GFT = quadgk_order_test(bound, step)
plot(1:length(GFT), GFT, label="GFT", title="impact of order(briefly sight)")



# find the optimal rtol and order
# full report
rtol_bound, order_bound, rtol_len = (1e-7, 1e-16), (5, 30), 1000
rtol_report, order_report = rtol_order_report(rtol_bound, order_bound, rtol_len)

min_std_rtol = minimum(rtol_report.:std)
index = findfirst(isequal(opt_rtol_std), rtol_report.:std)
rtol_opt = rtol_report.:rtol[index]
@show min_std_rtol
@show rtol_opt
plot(rtol_report.:rtol, rtol_report.:std, xlabel="rtol", label="std")

min_std_order = minimum(order_report.:std)
index = findfirst(isequal(opt_order_std), order_report.:std)
order_opt = order_report.:order[index]
@show min_std_order
@show order_opt
plot(order_report.:order, order_report.:std, xlabel="order", label="std")



# optimal adaptive gauss quadrature
W, W_err = quadgk(g, rlb, 1, rtol=rtol_opt, order=order_opt)
W_aut, W_err_aut = quadgk(g, rlb_aut, 1, rtol=rtol_opt, order=order_opt)
adaptive_GQ_GFT = W / W_aut
@show W_err, W_err_aut, adaptive_GQ_GFT
