# import module
using Interact, Plots, Distributions

# get the parameters
include("utils/GFT_param.jl")
include("utils/quaMCM.jl")
using .GFT_param_mod: rlb_aut, g
using .quaMCM_mod: prime_test

# plot the function form
plot(g, xlim=[rlb_aut, 1], label="", title="function form")


# select the prime
n = 10000
GFT_2, GFT_7, GFT_11, GFT_13, GFT_17, GFT_19 = zeros(n), zeros(n), zeros(n),
                                               zeros(n), zeros(n), zeros(n)
for prime in (2,7,11,13,17,19)
    if prime == 2
        GFT_2 = prime_test(2, n)
    elseif prime == 7
        GFT_7 = prime_test(7, n)
    elseif prime == 11
        GFT_11 = prime_test(11, n)
    elseif prime == 13
        GFT_13 = prime_test(13, n)
    elseif prime == 17
        GFT_17 = prime_test(17, n)
    else
        GFT_19 = prime_test(19, n)
    end
end

p1 = plot(1:n, GFT_2, label="prime=2")
p2 = plot(1:n, GFT_7, label="prime=7")
p3 = plot(1:n, GFT_11, label="prime=11")
p4 = plot(1:n, GFT_13, label="prime=13")
p5 = plot(1:n, GFT_17, label="prime=17")
p6 = plot(1:n, GFT_19, label="prime=19")
plot(p1, p2, p3, p4, p5, p6, layout=(2,3))

@manipulate for beg = 1:n
    plot(beg:n, GFT_11[beg:end], label="GFT",
         title="quasi monte carlo integration with prime = 11")
end

# use prime = 11 to do integration and generate the plot
opt_GFT_quaMCM = prime_test(11, 50000)
@show opt_GFT_quaMCM[end]
@show std(opt_GFT_quaMCM[10000:50000])
