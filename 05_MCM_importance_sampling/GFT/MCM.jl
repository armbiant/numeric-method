# import module
using Random, Distributions, Plots, DataFrames

# get the parameters
include("utils/GFT_param.jl")
using .GFT_param_mod: rlb, rlb_aut, g


# rewrite teacher's code using julia
Random.seed!(65536)  # seed of random seeds

# simulate for 1000 times, each time with a different seed
rng = floor.(Int64, rand(Uniform(0, 65535), 1000))
exper_num = length(rng)
GFT = zeros(exper_num);  # store the result

# ordinary MCM
for i = 1:exper_num
    # set seed
    Random.seed!(rng[i])
    # open economy case
    r_drw = rand(Uniform(rlb, 1), 10^6)  # generate r_drw
    tmp_w = mean(g.(r_drw))  # calculate the mean
    W = (1-rlb) * tmp_w
    #Autarky case
    r_drw_aut = rand(Uniform(rlb_aut, 1), 10^6)  # generate r_drw
    tmp_w_aut = mean(g.(r_drw_aut))  # calculate the mean
    W_aut = (1-rlb_aut) * tmp_w_aut

    GFT[i]= W / W_aut
end

@show minimum(GFT)
@show maximum(GFT)
@show mean(GFT)
@show std(GFT)

sim_rst = DataFrame(Times=1:exper_num, Seed_Used=rng, Result=GFT)

bar(GFT, label="")
