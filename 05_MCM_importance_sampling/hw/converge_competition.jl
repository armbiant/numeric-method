# question
#Consider the following integration problem:
#int_a^1 c\cdot x^{-k-1}(1-x)^{k+1}dx.
#Assume a = 10^{-5}$, $c=10^{-9}$, and $k=2$.


###############################################################################
#                              import module                                 #
##############################################################################
using StatsPlots, FastGaussQuadrature, HaltonSequences, Random, QuadGK,
      Distributions, Interact, LaTeXStrings, StaticArrays


###############################################################################
#                                basic setup                                 #
##############################################################################
g(x, c=1e-9, k=2) = c * x^(-k-1) * (1-x)^(k+1)

x_GL(a, b, t) = (a+b)/2 + (b-a)*t/2
f_GL(t, a=1e-5, b=1) = g(x_GL(a, b, t)) * ((b-a)/2)

x_MCM(a, b, t) = a + (b-a)*t
f_MCM(t, a=1e-5, b=1) = g(x_MCM(a, b, t)) * (b-a)


###############################################################################
#                                Gauss Legendre                              #
##############################################################################
function GL(trueVal::Float64; testNum::Int64=3000, prec::Int64=5)
    GL_res = zeros(testNum)
    compVal = round(trueVal, RoundDown, digits=prec)
    for i in 1:testNum
        xi, wi = gausslegendre(i)
        res = f_GL.(xi)' * wi
        GL_res[i] = res
        round(res, RoundDown, digits=5) == compVal && (return GL_res[1:i], i)
    end
    throw("not converge yet...")
end
GL(quadgk(g, 1e-5, 1)[1])[1][end]


###############################################################################
#                             ordinary Monte Carlo                           #
##############################################################################
# find the optimal seed(warning: intensive computation required)
function seedSelect_MCM(Seed::Vector, NodesNum::SVector, trueVal::Float64)
    seedNum, n = length(Seed), length(NodesNum)
    deviate = zeros(seedNum)
    for i in 1:seedNum
        println("#############################")
        println("### test out the $i seed  ###")
        println("#############################")
        res = zeros(SizedVector{n})
        for j in 1:n
            res[j] = mean(f_MCM.(rand(Xoshiro(Seed[i]), NodesNum[j])))
        end
        deviate[i] = abs(mean(res)-trueVal)
    end
    return seed = Seed[argmin(deviate)]
end

function MCM(NodesNum::SVector, trueVal::Float64; prec::Int64=2, seed::Int64=9139)
    n = length(NodesNum)
    MCM_deviate = zeros(SizedVector{n})
    compVal = round(trueVal, RoundDown, digits=prec)
    converge = -1
    for i in 1:n
        res = mean(f_MCM.(rand(Xoshiro(seed), NodesNum[i])))
        MCM_deviate[i] = abs(res-trueVal)
        if converge == -1 && round(res, RoundDown, digits=prec) == compVal
            converge = NodesNum[i]
        end
    end
    return MCM_deviate, converge
end


###############################################################################
#                              Quasi Monte Carlo                             #
##############################################################################
# find the optimal prime
function primeTest()
    Prime = 2, 5, 7, 11, 19
    NodesNum_2 = @SVector [524_287, 1_048_575, 2_097_151, 8_388_607, 67_108_863]
    NodesNum_5 = @SVector [390_624, 1_953_124, 9_765_624, 48_828_124]
    NodesNum_7 = @SVector [823_542, 5_764_800, 40_353_606]
    NodesNum_11 = @SVector [1_771_560, 19_487_170]
    NodesNum_19 = @SVector [2_476_098, 47_045_880]
    n_2, n_5, n_7, n_11, n_19 = length(NodesNum_2), length(NodesNum_5), length(NodesNum_7),
                                length(NodesNum_11), length(NodesNum_19)
    NodesNum = [NodesNum_2, NodesNum_5, NodesNum_7, NodesNum_11, NodesNum_19]
    res_primeTest = [zeros(SizedVector{n_2}), zeros(SizedVector{n_5}), zeros(SizedVector{n_7}),
                     zeros(SizedVector{n_11}), zeros(SizedVector{n_19})]
    for i in 1:length(Prime)
        target = NodesNum[i]
        for j in 1:length(target)
            res_primeTest[i][j] = mean(f_MCM.(Halton(Prime[i], length=target[j])))
        end
        println("prime = $(Prime[i]):")
        println(res_primeTest[i])
        println("------------------------------")
    end
end

function quaMCM(NodesNum::SVector, trueVal::Float64; prec::Int64=2, prime::Int64=2)
    n = length(NodesNum)
    compVal = round(trueVal, RoundDown, digits=prec)
    quaMCM_deviate = zeros(SizedVector{n})
    converge = -1
    for i in 1:n
        res = mean(f_MCM.(Halton(prime, length=NodesNum[i])))
        quaMCM_deviate[i] = abs(res-trueVal)
        if converge == -1 && round(res, RoundDown, digits=prec) == compVal
            converge = NodesNum[i]
        end
    end
    return quaMCM_deviate, converge
end


###############################################################################
#                             importance sampling                             #
##############################################################################
# find the optimal mu and sig
function test_mu_sig_random(NodesNum::SVector)
    n = length(NodesNum)
    Mu = 1e-5, 1e-5+1e-10, 1e-5+1e-9, 1e-5+1e-8, 1e-5+1e-7, 1e-5+1e-6, 2*1e-5,
         1e-5+1e-4, 1e-5+1e-3
    Sig = 1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7
    Random.seed!(1234)
    @manipulate for mu in Mu, sig in Sig
    res = zeros(SizedVector{n})
        d = truncated(Normal(mu, sig), 1e-5, 1.)
        f_IS(x) = g(x) / pdf(d, x)
        for i in 1:n
            res[i] = mean(f_IS.(rand(d, NodesNum[i])))
        end
        plot(NodesNum, res, ylim=[4.9, 5.1])
    end
end

function test_mu_sig_halton(NodesNum::SVector)
    n = length(NodesNum)
    Mu = 1e-5, 1e-5+1e-10, 1e-5+1e-9, 1e-5+1e-8, 1e-5+1e-7, 1e-5+1e-6, 2*1e-5,
         1e-5+1e-4, 1e-5+1e-3
    Sig = 1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7
    Random.seed!(1234)
    @manipulate for mu in Mu, sig in Sig
    res = zeros(SizedVector{n})
        d = truncated(Normal(mu, sig), 1e-5, 1.)
        f_IS(x) = g(x) / pdf(d, x)
        for i in 1:n
            sample = quantile.(d, Halton(2, length=NodesNum[i]))
            res[i] = mean(f_IS.(sample))
        end
        plot(NodesNum, res, ylim=[4.9, 5.1])
    end
end

# find the optimal seed(warning: extremely intensive computation required)
function seedSelect_IS(Seed::Vector, NodesNum::SVector, trueVal::Float64)
    seedNum, n = length(Seed), length(NodesNum)
    deviate = zeros(seedNum)
    d = truncated(Normal(1e-5+1e-4, 1e-4), 1e-5, 1)
    f_IS(x) = g(x) / pdf(d, x)
    for i in 1:seedNum
        println("#############################")
        println("### test out the $i seed  ###")
        println("#############################")
        res = zeros(SizedVector{n})
        Random.seed!(Seed[i])
        for j in n
            res[j] = mean(f_IS.(rand(d, NodesNum[j])))
        end
        deviate[i] = abs(mean(res)-trueVal)
    end
    return seed = Seed[argmin(deviate)]
end

function IS_random(NodesNum::SVector, trueVal::Float64; prec::Int64=2, seed::Int64=18708)
    n = length(NodesNum)
    compVal = round(trueVal, RoundDown, digits=prec)
    IS_deviate = zeros(SizedVector{n})
    converge = -1
    d = truncated(Normal(1e-5+1e-4, 1e-4), 1e-5, 1)
    f_IS(x) = g(x) / pdf(d, x)
    Random.seed!(seed)
    for i in 1:n
        res = mean(f_IS.(rand(d, NodesNum[i])))
        IS_deviate[i] = abs(res-trueVal)
        if converge == -1 && round(res, RoundDown, digits=prec) == compVal
            converge = NodesNum[i]
        end
    end
    return IS_deviate, converge
end

# use the inverse transform sampling method
function IS_halton(NodesNum::SVector, trueVal::Float64; prec::Int64=2)
    n = length(NodesNum)
    compVal = round(trueVal, RoundDown, digits=prec)
    IS_deviate = zeros(SizedVector{n})
    converge = -1
    d = truncated(Normal(1e-5+1e-3, 1e-3), 1e-5, 1)
    f_IS(x) = f_MCM(x) / pdf(d, x)
    for i in 1:n
        sample = quantile.(d, Halton(2, length=NodesNum[i]))
        res = mean(f_IS.(sample))
        IS_deviate[i] = abs(res-trueVal)
        if converge == -1 && round(res, RoundDown, digits=prec) == compVal
            converge = NodesNum[i]
        end
    end
    return IS_deviate, converge
end


###############################################################################
#     convergence competion(MCM, quasi MCM, importance samplint)             #
##############################################################################
function convergence_competition_1(MCM_deviate::SizedVector, quaMCM_deviate::SizedVector, IS_deviate::SizedVector)
    deviateMatrix = [MCM_deviate quaMCM_deviate IS_deviate]
    x = repeat(["0.5", "1", "2", "8.3", "67.1"], outer=3)
    label = repeat(["MCM", "Quasi MCM", "importance sampling"], inner=5)
    groupedbar(x, deviateMatrix, group=label, xlabel="nodes Used(million)",
               ylabel="deviation(absolute value)", title="deviation comparison",
               bar_width=0.5, lw=0, framestyle=:box)
end


###############################################################################
#  convergence competion(importance samplint sample from random and Haltonn)  #
############## ################################################################
function convergence_competition_2(IS_random_deviate::SizedVector, IS_halton_deviate::SizedVector)
    deviateMatrix = [IS_random_deviate IS_halton_deviate]
    x = repeat(["0.5", "1", "2", "8.3", "67.1"], outer=2)
    #x = repeat(["0.5", "1", "2", "67.1"], outer=2)
    label = repeat(["IS_random", "IS_halton"], inner=5)
    groupedbar(x, deviateMatrix, group=label, xlabel="nodes Used(million)",
               ylabel="deviation(absolute value)", title="deviation comparison",
               bar_width=0.5, lw=0, framestyle=:box)
end


###############################################################################
#                              driver function                               #
##############################################################################
function main()
    # get the blue print of the function
    #plot(g, xlim=[0.01, 1]) |> display
    #plot(g, xlim=[1e-5, 1e-3])
    #plot(f_GL, xlim=[-1, 1])
    #plot(f_MCM, xlim=[0.8, 1]) |> display

    trueVal = quadgk(g, 1e-5, 1)[1]

    # check the efficiency of the gauss legendre rule
    #GL_res, nodesUsed = GL(trueVal)  # nodesUsed=2033
    #@show nodesUsed
    #label = LaTeXString("\$\\int_{-1}^{1}f_{GL}(t)dt\$")
    #plot(1:nodesUsed, GL_res, xlabel="nodes num", ylabel="integral value", label=label,
         #legend=:bottomright, size=(700, 400))

    NodesNum = @SVector [524_287, 1_048_575, 2_097_151, 8_388_607, 67_108_863]

    # find the optimal seed
    #Random.seed!(65535)
    #Seed = floor.(Int64, rand(Uniform(0, 65535), 1000))
    #@show seedSelect_MCM(Seed, NodesNum, trueVal)  # 9139
    #@show seedSelect_IS(Seed, NodesNum, trueVal)  # 18708

    # find the optimal prime for Quasi Mote Carlo
    #primeTest()

    # find the optimal mu and sig for Importance Sampling random version
    #test_mu_sig_random(NodesNum)
    #NodesNum = @SVector [10, 1000, 5000, 10000]
    #test_mu_sig_halton(NodesNum)

    # converge competition of MCM and Quasi MCM, importance sampling
    #MCM_deviate, converge = MCM(NodesNum, trueVal)
    #quaMCM_deviate, converge = quaMCM(NodesNum, trueVal)
    #IS_deviate, converge = IS_random(NodesNum, trueVal)
    #@show converge
    #convergence_competition_1(MCM_deviate, quaMCM_deviate, IS_deviate)

    # converge competition of random and Halotn importance sampling
    IS_random_deviate, converge = IS_random(NodesNum, trueVal)
    IS_halton_deviate, converge = IS_halton(NodesNum, trueVal)
    @show IS_halton_deviate
    convergence_competition_2(IS_random_deviate, IS_halton_deviate)


    # converge competition of IS_halton with truncated exponenetial and normal
    #dn = truncated(Normal(1e-5+1e-3, 1e-3), 1e-5, 1)
    #de = truncated(Exponential(), 1e-5, 1)
    #IShal_tn, converge_tn = IS_halton(dn, NodesNum, trueval, prec=3)
    #IShal_te, converge_te = IS_halton(de, NodesNum, trueVal, prec=3)
    # @manipulate for alpha in 2.5:0.1:3, beta in 1e-6:1e-8:1-4
    #     pdfE(x) = pdf(Gamma(alpha, beta), x)
    #     #plot(f_MCM, label="f_MCM", xlim=[0, 1])
    #     plot(pdfE, xlim=[1e-2, 1], label="Gamma")
    # end
end

main()
