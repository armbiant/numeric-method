mutable struct simpleQua_param
    floor::Int64
    ceiling::Int64
    func::Function
    func_name::String
    true_val::BigFloat
    n::Int64
    simpleQua_param() = new()
end

SQparam = simpleQua_param()


function midpoint(p::simpleQua_param=SQparam)
    weight = (p.ceiling-p.floor) / p.n
    x = (p.floor+0.5weight) : weight : (p.ceiling-0.5weight)
    nodes = p.func.(x)
    weights = repeat([weight], outer=length(nodes))
    err = p.true_val - nodes' * weights
    return err, nodes, x, weights
end


function forward(p::simpleQua_param=SQparam)
    weight = (p.ceiling-p.floor) / p.n
    x = (p.floor+0.5weight) : weight : (p.ceiling-0.5weight)
    nodes = @.p.func(x - 0.5weight)
    weights = repeat([weight], outer=length(nodes))
    err = p.true_val - nodes' * weights
    return err, nodes, x, weights
end


function backward(p::simpleQua_param=SQparam)
    weight = (p.ceiling-p.floor) / p.n
    x = (p.floor+0.5weight) : weight : (p.ceiling-0.5weight)
    nodes = @.p.func(x + 0.5weight)
    weights = repeat([weight], outer=length(nodes))
    err = p.true_val - nodes' * weights
    return err, nodes, x, weights
end


function trapezoid(p::simpleQua_param=SQparam)
    weight = (p.ceiling-p.floor) / p.n
    x = p.floor : weight : p.ceiling
    nodes_used = p.func.(x)
    nodes = (nodes_used[1:p.n] .+ nodes_used[2:p.n+1]) ./ 2
    weights = repeat([weight], outer=length(nodes))
    err = p.true_val - nodes' * weights
    return err, nodes_used, x
end


function simpson(p::simpleQua_param=SQparam)
    segment_weight = (p.ceiling-p.floor) / p.n
    x_used = p.floor : segment_weight : p.ceiling
    start = repeat(x_used[1:p.n], inner=3)
    step = repeat([segment_weight/12, segment_weight/2,
                11segment_weight/12], outer=p.n)
    x = start .+ step
    adjusted = repeat([-segment_weight/12, 0, segment_weight/12], outer=p.n)
    nodes = @.p.func(x+adjusted)
    weights = repeat([segment_weight/6, 2segment_weight/3,
                    segment_weight/6], outer=p.n)
    try
        err = p.true_val - nodes' * weights
    catch
        return nodes' * weights
    end

    return err, nodes, x, weights
end


function SQplot(rule::String, portion::Int64, p::simpleQua_param=SQparam)
    @manipulate for n = 1:portion
        p.n =n
        if rule == "trapezoid"
            error, nodes, x = trapezoid(p)
            plot(x, zeros(length(x)), fillrange=nodes, fillalpha=0.35,
                 label="error=$(Float32(error))")
            plot!(x, nodes, shape=:circle, markersize=2, seriestype=:scatter,
                  label="")
        else
            if rule == "midpoint"
                error, nodes, x, weights = midpoint(p)
            elseif rule == "forward"
                error, nodes, x, weights = forward(p)
            elseif rule == "backward"
                error, nodes, x, weights = backward(p)
            else
                error, nodes, x, weights = simpson(p)
            end
            bar(x, nodes, bar_width=weights, fillalpha=0.35,
                label="error=$(Float32(error))")
        end
        plot!(SQparam.func, xlim=(0, 22), ylim=(0, 0.6),
            xticks=0:1:22, label=SQparam.func_name, xlabel="x", ylabel="f(x)",
            title="$(rule) rule quadrature", size=(650, 300))
    end
end


function lossAnalysis(count::Int64, p::simpleQua_param=SQparam)
    err_trap, err_mid, err_forw, err_back, err_simp = zeros(count), zeros(count), zeros(count), zeros(count), zeros(count)
    for i = 1:count
        p.n = i
        err_trap[i], err_mid[i], err_forw[i], err_back[i], err_simp[i] = trapezoid(p)[1],
            midpoint(p)[1], forward(p)[1], backward(p)[1], simpson(p)[1]
    end
    @manipulate for i=0:0.0001:0.1
        plot(1:count, err_trap, label="trapezoid loss")
        plot!(1:count, err_mid, label="midpoint loss")
        plot!(1:count, err_forw, label="forward loss")
        plot!(1:count, err_back, label="backward loss")
        plot!(1:count, err_simp, label="simpson loss", ylim=(-i, i))
    end
end
