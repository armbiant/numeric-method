module quadgk_mod
    export rtol_test, order_test, rtol_order_report

    using DataFrames: DataFrame
    using Distributions: minimum, maximum, mean, std
    using QuadGK: quadgk
    include("GFT_param.jl")
    using .GFT_param_mod: rlb, rlb_aut, g

    function rtol_test(bound::Tuple{Float64}, len::Int64)
        GFT = zeros(len)
        test_rtol = range(bound[1], bound[2], length=len)
        for rtol in test_rtol
            W, W_err = quadgk(g, rlb, 1, rtol=rtol)
            W_aut, W_err_aut = quadgk(g, rlb_aut, 1, rtol=rtol)
            GFT[i] = W / W_aut
        end
        return GFT
    end

    function order_test(bound::Tuple{Int64}, step::Int64)
        test_order = range(bound[1], bound[2], step=step)
        GFT = zeros(length(test_order))
        for order in test_ordr
            W, W_err = quadgk(g, rlb, 1, order=order)
            W_aut, W_err_aut = quadgk(g, rlb_aut, 1, order=order)
            GFT[i] = W / W_aut
        end
        return GFT
    end

    function rtol_order_report(rtol_bound::Tuple, order_bound::Tuple,
                               rtol_len::Int64)
        rtol = range(rtol_bound[1], rtol_bound[2], length=rtol_len)
        order = range(order_bound[1], order_bound[2], step=1)
        rtol_num = length(rtol)
        order_num = length(order)

        df_test_rtol = DataFrame(rtol=rtol)
        min_test_rtol = zeros(rtol_num)
        max_test_rtol = zeros(rtol_num)
        mean_test_rtol = zeros(rtol_num)
        std_test_rtol = zeros(rtol_num)

        for (i, rtol_i) in enumerate(rtol)
            GFT_test_rtol = zeros(order_num)
            for (j, order_j) in enumerate(order)
                W, W_err = quadgk(g, rlb, 1, rtol=rtol_i, order=order_j)
                W_aut, W_aut_err = quadgk(g, rlb_aut, 1, rtol=rtol_i, order=order_j)
                GFT = W / W_aut
                GFT_test_rtol[j] = GFT
            end
            min_test_rtol[i] = minimum(GFT_test_rtol)
            max_test_rtol[i] = maximum(GFT_test_rtol)
            mean_test_rtol[i] = mean(GFT_test_rtol)
            std_test_rtol[i] = std(GFT_test_rtol)
        end

        df_test_rtol.:min = min_test_rtol
        df_test_rtol.:max = max_test_rtol
        df_test_rtol.:mean = mean_test_rtol
        df_test_rtol.:std = std_test_rtol;

        df_test_order = DataFrame(order=order)
        min_test_order = zeros(order_num)
        max_test_order = zeros(order_num)
        mean_test_order = zeros(order_num)
        std_test_order = zeros(order_num)

        for (i, order_i) in enumerate(order)
            GFT_test_order = zeros(rtol_num)
            for (j, rtol_j) in enumerate(rtol)
                W, W_err = quadgk(g, rlb, 1, order=order_i, rtol=rtol_j)
                W_aut, W_aut_err = quadgk(g, rlb_aut, 1, order=order_i, rtol=rtol_j)
                GFT_test_order[j] = W / W_aut
            end
            min_test_order[i] = minimum(GFT_test_order)
            max_test_order[i] = maximum(GFT_test_order)
            mean_test_order[i] = mean(GFT_test_order)
            std_test_order[i] = std(GFT_test_order)
        end

        df_test_order.:min = min_test_order
        df_test_order.:max = max_test_order
        df_test_order.:mean = mean_test_order
        df_test_order.:std = std_test_order

        return df_test_rtol, df_test_order
    end

end  # end of the module
