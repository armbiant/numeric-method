{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "df0909db",
   "metadata": {},
   "source": [
    "# Numerical Integration: Part I\n",
    "&copy; Hung-Jen Wang (wangh@ntu.edu.tw), 2022 \n",
    "\n",
    "\n",
    "The type of integration that we often encounter may fit the general form of\n",
    "\n",
    "$$\\begin{aligned}\n",
    "I = \\int_a^b p(x) f(x) dx,\n",
    "\\end{aligned}$$\n",
    "\n",
    "where $p(x)$ is a nonnegative function which is often referred to as the *weight function*. For example, $p(x)$ could be the probability density function of a continuous random variable $X$ with the support $[a,b]$, such that $I=E[f(x)]$ is the expected value of $f(X)$. In some applications, $p(x)$ may not bear the interpretation of probability or be naturally understood as weights. That's fine. For instance, we could have $p(x)=1$, so that $I$ is the area under the $f(x)$ function between $a$ and $b$. \n",
    "\n",
    "Some integrals cannot be computed analytically, so we need to evaluate them numerically."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01b8b2e3",
   "metadata": {},
   "source": [
    "## Warm-up: Basic Rules for Numerical Approximation of Definite Integrals\n",
    "\n",
    "Let us use a simple example to illustrate the basic idea of numerical integrations. Given an interval $[a,b]$ and a function $g(x) = \\frac{1}{1+x}$, we would like to find the area under the curve over $[2,20]$:\n",
    "\n",
    "$$\\begin{aligned}\n",
    " I = \\int_a^b g(x) d x =  \\int_2^{20} \\frac{1}{1+x} dx.\n",
    "\\end{aligned}$$\n",
    "\n",
    "$I$ in this example has a closed-form solution which is $I = \\log(7) \\approx 1.9459101490553132$; we are going to numerically evaluate the integral anyway.\n",
    "\n",
    "Let's plot $g(x)$ to get a visual on the curve. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efe3304b",
   "metadata": {},
   "source": [
    "### import library and simple setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0c2bea9",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T14:28:44.111000+08:00",
     "start_time": "2022-03-23T06:28:43.603Z"
    }
   },
   "outputs": [],
   "source": [
    "using LaTeXStrings, Plots, Interact\n",
    "true_value = 1.9459101490553133051053527434432\n",
    "legend = LaTeXStrings.LaTeXString(\"\\$\\\\frac{1}{1+x}\\$\")  # \\$, \\\\: to type $, \\\n",
    "g(x) = 1/(1+x)\n",
    "xlim, ylim, fillalpha = (0, 22), (0, 0.6), 0.35  # xticks: portion to divide x\n",
    "xlabel, ylabel, size = \"x\", \"g(x)\", (650, 380)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9fa9302",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T14:28:46.257000+08:00",
     "start_time": "2022-03-23T06:28:45.575Z"
    }
   },
   "outputs": [],
   "source": [
    "plot(g, xlim=xlim, ylim=ylim, xticks=0:2:22, label=legend,\n",
    "     xlabel=xlabel, ylabel=ylabel, size=size)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab22e916",
   "metadata": {},
   "source": [
    "We'll first introduce a few simple methods, all of which are based on the idea of slicing the area under the curve into smaller pieces in the hope that each piece would be a good approximation of the corresponding area under the nonlinear curve. The integral is then the sum of these pieces.\n",
    "\n",
    "To fix idea, let's say that we divide $[2,20]$ into equally spaced intervals with the nodes of $[2,4,6,8,10,12,14,16,18,20]$. Using the nodes we may form 9 non-overlapping intervals of $[2,4]$, $[4,6]$, $\\ldots$, and $[18,20]$. The following three simple methods, namely the forward rectangular rule, the midpoint rule, and the trapezoid rule, all evaluate the area defined by the 9 intervals under the curve. They differ in how each of the interval-area is calculated.\n",
    "\n",
    "\n",
    "###  Forward Rectangular Rule\n",
    "\n",
    "In the forward rectangle rule (also called the left-hand rectangle rule), we use the *left* endpoint of an interval to calculate the corresponding rectangles of the interval. The size of the first rectangle is $(4-2)g(2) = (4-2)\\frac{1}{1+2}$ and the second one is $(6-4) g(4) = (6-4)\\frac{1}{1+4}$, and so on. Therefore, the integral is evaluated as \n",
    "\n",
    "\n",
    "$$\\begin{aligned}\n",
    "I = \\int_2^{20}\\frac{1}{1+x}dx  \\approx  (4-2)\\frac{1}{1+2} + (6-4)\\frac{1}{1+4} + \\ldots,                             \n",
    "\\end{aligned}$$\n",
    "Or, more generally,\n",
    "$$\\begin{aligned}\n",
    "I = \\int_a^{b} g(x) dx \\approx & \\sum_{i=1}^n \\omega_i  g(\\xi_i) \\\\\n",
    " = & \\frac{b-a}{n} \\sum_{i=1}^n g(\\xi_i),\n",
    "\\end{aligned}$$\n",
    "\n",
    "where in our example $[a,b]=[2,20]$, $n=9$,  $\\omega_i= (b-a)/n =2$ is the length of the interval, and $\\{\\xi_i\\} = \\{2,4,6,8,10,12,14,16,18 \\}$.\n",
    "\n",
    ">Because we use only one point to interpolate a given interval (e.g., $\\xi_1=2$ of the first interval), we say the rule uses a _**single interpolation point**_ in the approximation. \n",
    ">\n",
    "> In the above, we repeatedly apply the rule to all the sub-intervals within the domain, and so the method may be described as the _**composite**_ forward rectangular rule.\n",
    ">\n",
    "> The same use of terminology applies to the following rules introduced in the section.\n",
    "\n",
    "As shown in the following figure, the integral is evaluated as the sum of these rectangles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a51544b",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T14:29:32.292000+08:00",
     "start_time": "2022-03-23T06:29:32.246Z"
    }
   },
   "outputs": [],
   "source": [
    "x_i = 3:2:19\n",
    "nodes = x_i .- 1\n",
    "g_i = @.g(nodes)  # equals to g.(w_i.-1)\n",
    "\n",
    "\n",
    "bar(x_i, g_i, bar_width=2, fillalpha=fillalpha, label=\"\")  # fillalpha: color?\n",
    "plot!(g, xlim=xlim, ylim=ylim, xticks=0:2:22, label=legend, xlabel=xlabel, ylabel=ylabel,\n",
    "      title=\"Forward Rectangular Rule\", size=size)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9736aad7",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T01:11:14.957000+08:00",
     "start_time": "2022-03-22T17:11:14.516Z"
    }
   },
   "outputs": [],
   "source": [
    "#@manipulate for n in (2, 3, 6, 9, 18, 36, 90, 180)  \n",
    "@manipulate for n in 1:90\n",
    "    step = (20-2)/n\n",
    "    w_i = 2+step/2:step:20-step/2 \n",
    "    g_i = @.g(w_i-step/2)\n",
    "    res = step*sum(g_i)\n",
    "    err = res - true_value\n",
    "\n",
    "    bar(w_i, g_i, xticks=0:2:22, bar_width=step, fillalpha=fillalpha, label=\"area=$(res)\\n  err=$(err)\")\n",
    "    plot!(g, xlim=xlim, ylim=ylim, label=\"\", xlabel=xlabel, ylabel=ylabel,\n",
    "          title=\"Forward Rectangular Rule\", size=size)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efcbdbc1",
   "metadata": {},
   "source": [
    "### Midpoint Rule\n",
    "\n",
    "As you can see from the graph of the rectangular rule, the rectangle on the far left has the worse approximation about the area under the curve. In general, the forward rectangular rule may not work well in places where the curve is steep.\n",
    "\n",
    "The *midpoint rule* mitigates the problem. This rule also uses a single interpolation point, but the point is taken as the middle point of the two endpoints of the interval so as to compromise the (large) drop of function values between the endpoints. Following from the above example, the first rectangle is thus evaluated by $(4-2)*g((4+2)/2) = (4-2)*\\frac{1}{1+(4+2)/2}$ . Here, $\\frac{4+2}{2}$ is the midpoint between 2 and 4. In general,\n",
    "\n",
    "$$\\begin{aligned}\n",
    "I = \\int_a^b g(x) dx \\approx & \\sum_{i=1}^n \\omega_i  g(\\tilde{\\xi}_i) \\\\\n",
    " = &  \\frac{b-a}{n} \\sum_{i=1}^n g(\\tilde{\\xi}_i),\n",
    "\\end{aligned}$$\n",
    "\n",
    "where in our example $[a,b]=[2,20]$, $n=9$,  $\\omega_i= (b-a)/n =2$ is the length of the interval, and $\\{\\tilde{\\xi}_i\\} =\\{(\\xi_i + \\xi_{i+1})/2\\} = \\{3,5,7,9,11,13,15,17,19 \\}$.\n",
    "\n",
    "This rule also uses a single interpolating point, such as $\\tilde{\\xi}_i = 3$ of the first interval."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f245a3d",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-22T17:38:32.328000+08:00",
     "start_time": "2022-03-22T09:38:31.977Z"
    }
   },
   "outputs": [],
   "source": [
    "x_i = 3:2:19\n",
    "g_i = g.(x_i)\n",
    "\n",
    "bar(x_i, g_i, xticks=1:2:21, bar_width=2, fillalpha=fillalpha, label=\"\")\n",
    "plot!(g, xlim=xlim, ylim=ylim, label=legend, xlabel=xlabel, ylabel=ylabel,\n",
    "      tlitle=\"Midpoint Rule\", size=size) |> display"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1fd93c2",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T05:07:07.665000+08:00",
     "start_time": "2022-03-22T21:06:43.436Z"
    }
   },
   "outputs": [],
   "source": [
    "@manipulate for n in (2, 3, 6, 9, 18, 36, 90, 180)  \n",
    "    step = (20-2)/n\n",
    "    w_i = 2+step/2:step:20-step/2\n",
    "    g_i = g.(w_i)    \n",
    "    res = step * sum(g_i)\n",
    "    err = res - true_value\n",
    "\n",
    "    bar(w_i, g_i, xticks=1:1:21, bar_width=step, fillalpha=fillalpha, label=\"area=$(res)\\n  err=$(err)\")\n",
    "    plot!(g, xlim=xlim, ylim=ylim, label=\"\", xlabel=xlabel, ylabel=ylabel,\n",
    "          title=\"Midpoint Rule\", size=size)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6d92eb6",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Trapezoidal Rule\n",
    "\n",
    "The rectangular rule and the midpoint rule evaluate the rectangle use only a *single* interpolation point to calculate the area of a rectangle. We can certainly use more than one point to interpolate the area of a given interval, although the result may not always be better. The trapezoidal rule is an example, where it takes both of the endpoints of the interval to evaluate the area, as follows:\n",
    "\n",
    "$$\\begin{aligned}\n",
    "I = \\int_a^b g(x) dx \\approx & \\sum_{i=1}^n \\omega_i \\frac{ g(\\xi_i) + g(\\xi_{i+1})}{2} \\\\\n",
    "= &  \\frac{b-a}{n} \\sum_{i=1}^n \\frac{ g(\\xi_i) + g(\\xi_{i+1})}{2},\n",
    "\\end{aligned}$$\n",
    "\n",
    "where in our example $[a,b]=[2,20]$, $n=9$,  $\\omega_i= (b-a)/n =2$ is the length of the interval, and $\\{\\xi_i\\} = \\{2,4,6,8,10,12,14,16,18 \\} $. You are encourage to figure out the geometric representation of this rule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dab59e92",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-22T17:39:04.483000+08:00",
     "start_time": "2022-03-22T09:39:03.389Z"
    }
   },
   "outputs": [],
   "source": [
    "x_i = 2:2:20\n",
    "g_i = @.g(x_i)\n",
    "\n",
    "plot(x_i, zeros(length(x_i)), fillrange=g_i, fillalpha=fillalpha, label=\"\")\n",
    "plot!(x_i, g_i, xticks=0:2:22, shape=:circle, markersize=2, label=\"\", seriestype = :scatter)\n",
    "plot!(g, xlim=xlim, ylim=ylim, label=legend, xlabel=\"x\", ylabel=\"g(x)\",\n",
    "      title=\"Trapezoidal Rule\", size=size) |> display"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3e0bf8e",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T05:07:25.628000+08:00",
     "start_time": "2022-03-22T21:07:24.871Z"
    }
   },
   "outputs": [],
   "source": [
    "@manipulate for n in (2, 3, 6, 9, 18, 36, 90, 180)  \n",
    "    step = (20-2)/n\n",
    "    w_i = 2:step:20 \n",
    "    g_i = @.g(w_i)\n",
    "    res = (step/2)*sum(g_i[1:n] .+ g_i[2:n+1])\n",
    "    err = res - true_value\n",
    "    \n",
    "    plot(w_i, zeros(length(w_i)), fillrange=g_i, fillalpha=fillalpha, label=\"area=$(res)\\n  err=$(err) \")\n",
    "    plot!(w_i, g_i, xticks=0:2:22, shape=:circle, markersize=2, label=\"\", seriestype = :scatter)\n",
    "    plot!(g, xlim=xlim, ylim=ylim, label=\"\", xlabel=xlabel, ylabel=ylabel,\n",
    "          title=\"Trapezoidal Rule\", size=size)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cd16829",
   "metadata": {},
   "source": [
    "### A Few Remarks\n",
    "\n",
    "- You may have noticed the somewhat surprising fact that the midpoint rule is actually twice as accurate as the trapezoidal rule, meaning that the error of the former is about half of the latter's error. This fact is directly observed from the numerical examples above and it can also be proved mathematically. Note also that the two rules' errors have the opposite signs. \n",
    "- We can exploit this property by combining these two methods (in such a way that errors from the methods would cancel out each other) to achieve much more efficient algorithms. In fact, the _**Simpson's rule**_ can be interpreted as the combination of the midpoint and the trapezoidal rules and is thus more efficient.\n",
    "\n",
    "- The (composite) _**Simpson's rule**_ uses three interpolating points, which may be expressed as:\n",
    "$$\\begin{aligned}\n",
    "I = \\int_a^b f(x) dx \\approx   \\frac{b-a}{6n} \\sum_{i=1}^n \\left[ f(\\xi_i) + 4f\\left(\\frac{\\xi_i + \\xi_{i+1}}{2}\\right) + f(\\xi_{i+1}) \\right].\n",
    "\\end{aligned}$$ \n",
    "  - It uses three points to interpolate a given interval: $\\xi_i, (\\xi_i + \\xi_{i+1})/2$, and $\\xi_{i+1}$.\n",
    "  - The coefficients (or, _**weights**_) of these three points are $\\frac{b-a}{6n}$, $\\frac{2(b-a)}{3n}$, and $\\frac{b-a}{6n}$, which can be shown as the roots of an approximating polynomials.\n",
    "  - It is interesting to note that the result of the Simpson's rule using $n$ nodes ($I^{S(n)}$) is the weighted average of the results from the trapezoidal rule and the midpoint rule both using $n/2$ nodes. That is,\n",
    "$$\\begin{aligned}\n",
    "I^{S(n)} = \\frac{1}{3}I^{T\\left(\\frac{n}{2}\\right)} + \\frac{2}{3}I^{M\\left(\\frac{n}{2}\\right)}.\n",
    "\\end{aligned}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3fb2c60e",
   "metadata": {},
   "source": [
    "### implement Trapezoidal and midpoint rule simultaneously in one plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a2054af",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-03-23T15:18:02.573000+08:00",
     "start_time": "2022-03-23T07:17:24.725Z"
    }
   },
   "outputs": [],
   "source": [
    "using LaTeXStrings, Plots, Interact\n",
    "\n",
    "\n",
    "mutable struct Simp_param\n",
    "    f::Function\n",
    "    range::Tuple\n",
    "    true_val::BigFloat\n",
    "    n::Int64\n",
    "    Simp_param(func, range, true_val) = new(func, range, true_val)\n",
    "end\n",
    "\n",
    "\n",
    "function Simp_integral(p::Simp_param)\n",
    "    floor, ceiling = p.range[1], p.range[2]\n",
    "    step = (ceiling-floor) / p.n\n",
    "    start = repeat((floor:step:ceiling)[1:p.n], inner=3)  # range(floor:step:ceiling) have n+1 element\n",
    "    sub_step = repeat([step/12, step/2, 11*step/12], outer=p.n)  # how to find the nodes\n",
    "    x = start .+ sub_step\n",
    "    adjust = repeat([-step/12, 0, step/12], outer=p.n)  # forward and backward need to be adjusted\n",
    "    y = @.p.f(x+adjust)\n",
    "    width = repeat([step/6, 2*step/3, step/6], outer=p.n)\n",
    "    res = y' * width\n",
    "    err = res - p.true_val\n",
    "    return x, y, width, res, err\n",
    "end\n",
    "\n",
    "\n",
    "function main()\n",
    "    function_form = LaTeXStrings.LaTeXString(\"\\$\\\\frac{1}{1+x}\\$\")\n",
    "    f(x)=1/(1+x)\n",
    "    parameters = Simp_param(f, (2,20), BigFloat(log(7)))\n",
    "    @manipulate for n in (2, 3, 6, 9, 18, 36, 90, 180)  \n",
    "        parameters. n = n\n",
    "        X, Y, bar_width, res, err = Simp_integral(parameters)\n",
    "        bar(X, Y, bar_width=bar_width, fillalpha=0.35, label=\"area=$(res)\\nerr=$(err)\")\n",
    "        plot!(f, xlim=(0, 22), ylim=(0, 0.6), xtick=0:1:22, label=function_form, xlabel=\"x\", ylabel=\"g(x)\",\n",
    "              title=\"trapezoidal and midpoint rule\", size=(650, 380))\n",
    "    end\n",
    "end\n",
    "\n",
    "\n",
    "main()"
   ]
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Julia 1.7.2",
   "language": "julia",
   "name": "julia-1.7"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.7.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "192.54px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
