module GFT_param_mod
    export rlb, rlb_aut, g, domain_trans

    rlb = 5e-6
    rlb_aut  = 1e-6
    # target function
    g(r_drw, kappa_z=1.56, epsilon=1.667) =
            r_drw^((-kappa_z/(epsilon-1))-1) *
            (1-r_drw)^(((kappa_z-1)/(epsilon-1))-1)
    # domain transformation rule
    function domain_trans(floor::Float64, ceiling::Float64)
        x(t) = floor + (ceiling-floor)*t
        jcb(t) = ceiling-floor
        return f(t) = g(x(t)) * jcb(t)
    end
end # end of the module
